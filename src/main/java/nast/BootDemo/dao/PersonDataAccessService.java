package nast.BootDemo.dao;

import nast.BootDemo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Dependency injection occurs in service.PersonService's constructor.
 */
@Repository("postgres")
public class PersonDataAccessService implements PersonDao {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     *
     * @param id a Person's id
     * @param person a new Person object
     * @return Return 0 if success, otherwise 1.
     */
    @Override
    public int insertPerson(UUID id, Person person) {
        final String SQL = "INSERT INTO person (id, name) VALUES (?, ?)";
        jdbcTemplate.update(SQL, id, person.getName());
        return 0;
    }

    @Override
    public List<Person> selectAllPeople() {
        final String SQL = "SELECT id, name FROM person";
        return jdbcTemplate.query(SQL, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String name = resultSet.getString("name");
            return new Person(id, name);
        });
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        final String SQL = "SELECT id, name FROM person WHERE id = ?";
        Person person = jdbcTemplate.queryForObject(
                SQL,
                new Object[]{id},
                (resultSet, i) -> {
                    UUID personId = UUID.fromString(resultSet.getString("id"));
                    String name = resultSet.getString("name");
                    return new Person(personId, name);
                });
        return Optional.ofNullable(person);
    }

    @Override
    public int deletePersonById(UUID id) {
        final String SQL_DELETE_BY_ID = "DELETE FROM person WHERE id = ?";

        if (selectPersonById(id).isPresent()) {
            jdbcTemplate.update(SQL_DELETE_BY_ID, id);
            return 0;
        }

        return 1; // Delete failed (id not found)
    }

    /**
     *
     * @param id Id of an existing person
     * @param updPerson A new, updated person
     * @return 0 if updated successfully; 1 if update failed.
     */
    @Override
    public int updatePersonById(UUID id, Person updPerson) {
//        final String PERSON_EXISTS_SQL = "SELECT EXISTS (SELECT 1 FROM person WHERE id = ?)";
        final String UPDATE_PERSON_SQL = "UPDATE person SET name = ? WHERE id = ?";

        // If the person exists, update the record
        if (selectPersonById(id).isPresent()) {
            jdbcTemplate.update(UPDATE_PERSON_SQL, updPerson.getName(), id);
            return 0;
        }

        return 1; // Update failed
    }
}
