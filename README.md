# Spring Boot basics tutorial

## Objectives

- Create a simple REST API CRUD using an in-memory database;
- Then replace the in-memory database with a postgres database in a docker container.

## Links

- [Video tutorial](https://www.youtube.com/watch?v=vtPkZShrvXQ)

## API

`{{baseURL}} = localhost:8080`

#### Add a new person

```yaml
url: "{base_url}/api/v1/person"
method: POST
  
headers:
    Content-Type: application/json  
  
json:
    {
    	name: "Pupa"
    }
```
Success response:

```yaml
status_code: 200
```

#### Get a list of all people

```yaml
url: "{base_url}/api/v1/person"
method: GET
```
Success response:

```yaml
status_code: 200
response_body:
    [
        {
            "id": "7ecf179f-5166-4319-a203-c8f3a4ab20a5",
            "name": "Pupa"
        }
    ]
```

#### Get a person by id

`{personID}` is taken from the response of a get a list of all people request. 

```yaml
url: "{base_url}/api/v1/person/{personID}"
method: GET
```
Success response:

```yaml
status_code: 200
response_body:
    [
        {
            "id": "7ecf179f-5166-4319-a203-c8f3a4ab20a5",
            "name": "Pupa"
        }
    ]
```

#### Update a person

`{personID}` is taken from the response of a get a list of all people request. 

```yaml
url: "{base_url}/api/v1/person/{personID}"
method: PUT
  
headers:
	Content-Type: application/json  
  
json:
    {
        name: "Lupa"
    }
```
Success response:

```yaml
status_code: 200
response_body: 0
```

Response if a person is not found:

```yaml
status_code: 200
response_body: 1
```

or

```yaml
status_code: 200
response_body: -1
```

 

#### Delete a person

`{personID}` is taken from the response of a get a list of all people request. 

```yaml
url: "{base_url:s}/api/v1/person/{personID}"
method: DELETE
```
Success response:

```yaml
status_code: 200
response_body: 0
```

Response if a person is not found:

```yaml
status_code: 200
response_body: 1
```
